import discord
import random
import settings
from io import StringIO
from discord.ext import commands

bot = commands.Bot(command_prefix=settings.command_prefix, description=settings.description)
support_channel_count = 100
bot_shutdown = False

@bot.event
async def on_ready():
    print("We're logged in as " + bot.user.name)


@bot.event
async def on_message(message):
    if message.content == "":
        return #The message was blank, let's ignore it.
    if message.channel.is_private:
        if message.content.startswith(settings.command_prefix + settings.shutdown_word):
            pass_given = message.content[len(settings.command_prefix + settings.shutdown_word):].strip()
            if(pass_given == settings.admin_pass):
                global bot_shutdown
                bot_shutdown = True
                await bot.close()
        return
    if message.content.startswith(settings.command_prefix + settings.support_word):
        global support_channel_count
        message_content = message.content[len(settings.command_prefix + settings.support_word):]
        new_channel = await bot.create_channel(message.server, "Support_" + str(support_channel_count), type=discord.ChannelType.text)
        support_channel_count += 1

        try:
            await bot.edit_channel(new_channel,topic = message.author.id)
            info_msg = await bot.send_message(new_channel, "<@" + message.author.id + "> please proceed to use this channel for any further support with the topic you asked about.")
            question_msg = await bot.send_message(new_channel, "<@" + message.author.id + "> asked: `" + message_content + "`")
            await bot.send_message(new_channel, "<@" + message.author.id + "> when you are satisfied with your level of support please type **!solved** to mark your question as solved.")
            await bot.pin_message(info_msg)
            await bot.pin_message(question_msg)
            await bot.delete_message(message)
        except Exception as e:
            print(e)
            return

    if message.content.startswith(settings.command_prefix + settings.solved_word):
        user = message.author
        channel = message.channel
        # Let's check if the channels topic(we set earlier to the user id) matches this user's id
        if channel.topic == str(user.id): #We have to turn the user.id to a string, because it is originally a number and the number 1 does not equal the string "1"
            try:
                temp_file = StringIO()
                async for msg in bot.logs_from(channel, limit=500):
                    temp_file.write("[" + str(msg.timestamp) + "] " + msg.author.name + ": " + msg.content + "\r\n")
                temp_file.seek(0) #Set the "cursor" back to the begining of the file
                await bot.send_file(user, temp_file, filename=channel.name + ".txt", content="<@" + user.id + "> Here is a quick log of your closed support channel :)")
                temp_file.close()
                await bot.delete_channel(channel)
            except Exception as e:
                print(e)
                return

while(bot_shutdown != True):
    bot.run(settings.TOKEN)
